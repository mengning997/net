
#include<stdio.h> 			/* perror */
#include<stdlib.h>			/* exit	*/
#include<sys/types.h>		/* WNOHANG */
#include<sys/wait.h>		/* waitpid */
#include<string.h>			/* memset */
#include<pthread.h>

#include "socketwrapper.h" 	/* socket layer wrapper */
#include "parser.h"			/* ChatSys PDU parser   */
#include "event.h"
#include "msgq.h"

#include "chatsys.h"

#define	true		1
#define false		0
#define OK			0

int AddUserToList(int Connfd,char* Msg,int MsgLen);

int main() 
{
	tChatSysMsg Msg;
	Debug("Server Start...\n");
	if(CspInit(INIT_AS_SERVER)!= SUCCESS)
		printf("CspInit failure!\n");
	while(true)
	{
#if 1
		CspRecv(&Msg);
		printf("Msg.Connfd:%d\n",Msg.Connfd);
	 	printf("Msg.Version:%d\n",Msg.Version);
		printf("Msg.MsgType:%d\n",Msg.MsgType);
		printf("Msg.SerialNumber:%d\n",Msg.SerialNumber);
		printf("Msg.MsgLen:%d\n",Msg.MsgLen);
		Msg.Msg[Msg.MsgLen-1]='\0';
		printf("Msg.Msg:%s\n",Msg.Msg);	
		if(Msg.MsgType	== MT_LOGIN_MSG)
		{
			AddUserToList(Msg.Connfd,Msg.Msg,Msg.MsgLen);	
			continue;
		}
		else if(Msg.MsgType	== MT_CHAT_MSG)	
		{
			SendMsgToAllUsers(Msg);	
		}
		/*CspSend(&Msg);*/
#endif
	}
	CspShut();
	
}

int AddUserToList(int Connfd,char* Msg,int MsgLen)
{
	Msg[MsgLen-1]='\0';
	printf("User:%s Login!!!\n",Msg);	
}
