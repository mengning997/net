
/********************************************************************/
/* Copyright (C) SSE-USTC, 2010                                     */
/*                                                                  */
/*  FILE NAME             :  chatsys.c                              */
/*  PRINCIPAL AUTHOR      :  Mengning                               */
/*  SUBSYSTEM NAME        :  ChatSys                                */
/*  MODULE NAME           :  ChatSys                                */
/*  LANGUAGE              :  C                                      */
/*  TARGET ENVIRONMENT    :  ANY                                    */
/*  DATE OF FIRST RELEASE :  2010/12/10                             */
/*  DESCRIPTION           :  implement of ChatSys Protocol          */
/********************************************************************/

/*
 * Revision log:
 *
 * Created by Mengning,2010/12/10
 *
 */

#include <string.h>			/* memcpy */
#include <stdio.h>
#include <pthread.h>

#include "chatsys.h"
#include "socketwrapper.h" 	/* socket layer wrapper */
#include "event.h"
#include "msgq.h"
#include "parser.h"

typedef struct QMSG
{
	tQueueNode Node;
	tChatSysMsg data;
	
}tQMSG;

tQueue Q;
tEvent E;

/*
 * Recv msg from a specific Client
 */
void RecvThread(int fd);
/*
 * Accept Client Connections
 */
void AcceptClientThread(int *socketfd);
/*
 * ChatSys protocol initialize
 * if server,return SUCCESS/FAILURE
 * if client,return socket fd/FAILUE
 */
int CspInit(int cs)
{
	Debug("CspInit start...\n");
	pthread_t pid;
	int sockfd;            		   /* 监听端口，数据端口 */
	struct sockaddr_in addr;       /* 地址信息 */  
	/* Initial Q */
	QueueCreate(&Q);
	/* Init Event */
	EventInit(&E,0);
	addr.sin_family = AF_INET;
	addr.sin_port = Htons(SERVER_PORT);         /* 网络字节顺序 */
	Debug("Inet_aton start...\n");
	Inet_aton(SERVER_IP_ADDR,&(addr.sin_addr));     /* 自动填本机IP */
	memset(&(addr.sin_zero),0, 8);            /* 其余部分置0 */
	if ((sockfd = Socket(AF_INET, SOCK_STREAM, 0)) == FAILURE) 
	{
		perror("socket");  
	}
	if(cs == INIT_AS_SERVER)
	{
		Debug("Bind start...\n");
		if ( Bind(sockfd, (struct sockaddr *)&addr, sizeof(struct sockaddr)) == FAILURE)	
		{
			perror("bind");
		}	
		Debug2("addr.sin_addr: %s\n", Inet_ntoa(addr.sin_addr));	
		if (Listen(sockfd, BACKLOG) == FAILURE) 
		{
			perror("listen");
		}
		Debug("pthread_create start...\n");
		Debug2("sockfd:%d\n",sockfd);
		int *pfd = malloc(sizeof(int));
		*pfd = sockfd;
		if(pthread_create(&pid, NULL,(void*)AcceptClientThread, (void*)pfd) != SUCCESS)
			perror("pthread_create");
		Debug2("sockfd:%d\n",*pfd);		
	}
	else if(cs == INIT_AS_CLIENT)
	{
		if (Connect(sockfd, (struct sockaddr *)&addr, sizeof(struct sockaddr)) == FAILURE) 
		{
			perror("connect");
		}
		if(pthread_create(&pid, NULL,(void*)RecvThread, (void*)sockfd) != SUCCESS)
			perror("pthread_create");
		return sockfd;		
	}
	return SUCCESS;
}
/*
 * ChatSys protocol close a connection
 */
int CspCloseConnection(int fd)
{
	Close(fd);
	return SUCCESS;
}
/*
 * ChatSys protocol shutdown
 */
int CspShut()
{
#if 0
	/* Delete Q */
	QueueDelete(&Q);
	/* Shut Event */
	EventShut(&E);
#endif
	return SUCCESS;
}

/*
 * CspRecv a msg base on ChatSys protocol
 */
int CspRecv(tChatSysMsg * Msg)
{
	tQMSG * pTempMsg;
	if(Msg == NULL)
		return FAILURE;
	/* if no msg in Q,waiting here */
	WaitEvent(&E);
	QueueOutMsg(&Q,(tQueueNode **)&pTempMsg);
	/* Msg MUST be malloced */
	memcpy(Msg,&pTempMsg->data,sizeof(tChatSysMsg));
	/* Malloc it in ServForClient */
	free(pTempMsg);	
	return SUCCESS;
}
/*
 * CspSend a msg base on ChatSys protocol
 */
int CspSend(tChatSysMsg * Msg)
{
	Debug("CspSend start...\n");
	static int sn = 1;
	if(Msg == NULL)
		return FAILURE;
	int sockfd = Msg->Connfd;
	char pdu[MAX_NUM_STR];
	if(Msg->MsgType != MT_ACK_MSG)
		Msg->SerialNumber = ++sn;
	ChatSysPduFormat(pdu,Msg);
	if (Send(sockfd,pdu,MAX_NUM_STR, 0) == FAILURE)
		perror("send");
	Debug("CspSend end!\n");
	return SUCCESS;
}

/*****************************************/
/* Inernal Function					     */
/*****************************************/
/*
 * Accept Client Connections
 */
void AcceptClientThread(int *fd)
{
	int sockfd = *fd;
	Debug2("AcceptClientThread-sockfd:%d\n",*fd);
	free(fd);
	pthread_t pid;	
	int sin_size;
	int new_fd;
	struct sockaddr_in  their_addr;
	sin_size = sizeof(struct sockaddr_in);
	while(1) 
	{
		new_fd = Accept(sockfd, (struct sockaddr *)&their_addr, &sin_size);
		if (new_fd == -1) 
		{
				perror("accept");
		}
		printf("Got connection from %s\n", Inet_ntoa(their_addr.sin_addr));		
		if(pthread_create(&pid, NULL,(void*)RecvThread, (void*)new_fd) != SUCCESS)
			perror("pthread_create");
		
	}	
}
/*
 * Recv msg from a specific Client
 */
void RecvThread(int fd)
{
	Debug2("ServForClient-sockfd:%d\n",fd);
	char pdu[MAX_NUM_STR];
	int numbytes;
	tChatSysMsg AckMsg;
	while(1)
	{
		if ((numbytes=Recv(fd,pdu,MAX_NUM_STR,0))==FAILURE) 
		{
			perror("recv");
		}
		/* memory alloc,WARNNING:free */
		tQMSG *pMsg = malloc(sizeof(tQMSG));
		ChatSysPduParser(pdu,&(pMsg->data));
		pMsg->data.Connfd = fd;
		/* Drop the ACK Msg  */
		if(pMsg->data.MsgType == MT_ACK_MSG)
		{
			free(pMsg);
			continue;
		}		
		/* Provide the MSG to APP layer,
		   Msg+Event Method */
		QueueInMsg(&Q,(tQueueNode *)pMsg);
		SentEvent(&E);
		/* Send the ACK  Msg */
		AckMsg = pMsg->data;
		AckMsg.MsgType = MT_ACK_MSG;
		AckMsg.SerialNumber = pMsg->data.SerialNumber;
		AckMsg.MsgLen = 0;
		CspSend(&AckMsg);
		/* close the client connection  */
		if(pMsg->data.MsgType == MT_LOGOUT_MSG)
		{
			Debug("RecvThread:Logout\n");
			return;
		}
	}
}


