
/********************************************************************/
/* Copyright (C) SSE-USTC, 2010                                     */
/*                                                                  */
/*  FILE NAME             :  chatsys.h                              */
/*  PRINCIPAL AUTHOR      :  Mengning                               */
/*  SUBSYSTEM NAME        :  ChatSys                                */
/*  MODULE NAME           :  ChatSys                                */
/*  LANGUAGE              :  C                                      */
/*  TARGET ENVIRONMENT    :  ANY                                    */
/*  DATE OF FIRST RELEASE :  2010/12/10                             */
/*  DESCRIPTION           :  interface of ChatSys Protocol          */
/********************************************************************/

/*
 * Revision log:
 *
 * Created by Mengning,2010/12/10
 *
 */

#ifndef _CHAT_SYS_H_
#define _CHAT_SYS_H_

#define INIT_AS_SERVER	1
#define INIT_AS_CLIENT	2
#define MAX_NUM_STR 	1024
#define SUCCESS 		0
#define FAILURE 		(-1)
#define SERVER_IP_ADDR	"127.0.0.1"
#define SERVER_PORT		3003
#define BACKLOG 		10                 	/* listen的请求接收队列长度 */
/* Version */
#define CHAT_SYS_VERSION 	1 
/* Msg Type（MT） DEFINES */
#define MT_ERROR_MSG		0
#define MT_ACK_MSG		 	1
#define MT_LOGIN_MSG	 	2
#define MT_CHAT_MSG			3
#define MT_LOGOUT_MSG		4
#define MT_HOLD_MSG			5
/* Trace on/off */
#define Debug(a) 			/*printf(a)*/
#define Debug2(a,b) 		/*printf(a,b) */

typedef struct ChatSysMsg
{
	int		Connfd;
	char 	Version;
	char 	MsgType;
	char	SerialNumber;
	int		MsgLen;
	char	Msg[MAX_NUM_STR];	
}tChatSysMsg;



/*
 * ChatSys protocol initialize
 */
int CspInit(int cs);

/*
 * ChatSys protocol close a connection
 */
int CspCloseConnection(int fd);

/*
 * ChatSys protocol shutdown
 */
int CspShut();

/*
 * CspRecv a msg base on ChatSys protocol
 */
int CspRecv(tChatSysMsg * Msg);
/*
 * CspSend a msg base on ChatSys protocol
 */
int CspSend(tChatSysMsg * Msg);
#endif /* _CHAT_SYS_H_ */


