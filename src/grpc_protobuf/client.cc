#include <iostream>
#include <memory>
#include <string>

#include <grpcpp/grpcpp.h> // 引入头文件

// protobuf生成的头文件，包含两个信息和应用的头文件
#include "msg.grpc.pb.h"
#include "msg.pb.h"

// 这是通用工具
using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;



// service MsgService { // 定义服务, 流数据放到括号里面
//   rpc GetMsg (MsgRequest) returns (MsgResponse){}
// }
 
// message MsgRequest { // 请求的结构
//   int32 num1 = 1;
//   int32 num2 = 2;
// }
 
// message MsgResponse { // 回应的结果
//   int32 sum = 1;
// }


class MsgServiceClient {
 public:
  MsgServiceClient(std::shared_ptr<Channel> channel)
      : stub_(MsgService::NewStub(channel)) {}


  MsgResponse GetMsg(int num1, int num2) {
    // 请求数据数据格式化到request
    MsgRequest request; 
    request.set_num1(num1);
    request.set_num2(num2);

    // 服务器返回端
    MsgResponse reply;

    //客户端上下文。它可以用来传递额外的信息
    //服务器和/或调整某些RPC行为。
    ClientContext context;

    // The actual RPC.
    Status status = stub_->GetMsg(&context, request, &reply);

    // Act upon its status.
    if (status.ok()) {
      return reply;  // reply.sum();
    } else {
      std::cout << status.error_code() << ": " << status.error_message()
                << std::endl;
      return reply;
    }
  }

 private:
  std::unique_ptr<MsgService::Stub> stub_;
};

int main(int argc, char** argv) {
  MsgServiceClient z_msg(grpc::CreateChannel(
      "localhost:50051", grpc::InsecureChannelCredentials()));
  int num1,num2;
  std::cout << "please input two num:" << std::endl;
  std::cin >> num1 >> num2;
  MsgResponse reply = z_msg.GetMsg(num1,num2);
  std::cout<<reply.sum()<<std::endl;
  // std::cout << "Greeter received: " << reply << std::endl;
  return 0;
}


