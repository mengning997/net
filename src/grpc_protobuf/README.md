# gRPC & Protobuf 快速编程指南

### 一、环境搭建

#### 1.设置安装目录

选择一个目录来保存本地安装的软件包。这里我们选择安装在.local目录下，方便后续操作。

```bash
$ export MY_INSTALL_DIR=$HOME/.local
```

将安装目录添加到环境变量中

```bash
$ export PATH="$MY_INSTALL_DIR/bin:$PATH"
```

#### 2.安装cmake

```bash
$ sudo apt install -y cmake
```

需要注意的是本实验所要求的cmake版本必须大于3.13

cmake安装教程[cmake安装教程](https://cmake.org/install)

#### 3.安装构建grpc所需要的基本工具

```bash
$ sudo apt install -y build-essential autoconf libtool pkg-config
```

#### 4.clone源代码

从github上下载源代码以及子模块，其中就包含了protobuf模块

```bash
$ git clone --recurse-submodules -b v1.58.0 --depth 1 --shallow-submodules https://github.com/grpc/grpc
```

#### 5.编译安装gRPC

```bash
$ cd grpc
$ mkdir -p cmake/build
$ pushd cmake/build
$ cmake -DgRPC_INSTALL=ON \
      -DgRPC_BUILD_TESTS=OFF \
      -DCMAKE_INSTALL_PREFIX=$MY_INSTALL_DIR \
      ../..
$ make -j 4
$ make install
$ popd

```

#### 6.构建官方测试案例

切换到测试案例所在目录

```bash
$ cd examples/cpp/helloworld
```

编译测试案例

```bash
$ mkdir -p cmake/build
$ pushd cmake/build
$ cmake -DCMAKE_PREFIX_PATH=$MY_INSTALL_DIR ../..
$ make -j 8
```

运行

```bash
$ ./greeter_server
$ ./greeter_client
```

结果

服务端：Server listening on 0.0.0.0:50051

客户端：Greeter received: Hello world

出现上述结果即为安装成功



### 二、编写示例代码

示例代码为在客户端输入两个整数，服务端将两个数相加，并返回结果。

#### 1.protobuf文件

创建protobuf文件，根据以下命令生成中间文件

```bash
$ protoc -I=. --grpc_out=. --plugin=protoc-gen-grpc=`which grpc_cpp_plugin` msg.proto
$ protoc -I=. --cpp_out=. msg.proto
```

#### 2.服务端

```c++
class MyMsgService final : public MsgService::Service {
  Status GetMsg(ServerContext* context, const MsgRequest* request,
                  MsgResponse* reply) override {

    std::string str1("Hello ");
    reply->set_sum(request->num1() + request->num2()); // 给reply.sum赋值
    
    return Status::OK;
  }
};
```

**类声明**：`class MyMsgService final : public MsgService::Service`

- 定义了一个名为 `MyMsgService` 的类，继承自 gRPC 自动生成的 `MsgService::Service`。这个类会用于实现你在 protobuf 文件中定义的服务。

**GetMsg 函数**：`Status GetMsg(ServerContext* context, const MsgRequest* request, MsgResponse* reply) override`

- 这是由 gRPC 自动生成的用于处理客户端请求的函数。在这个函数中，你可以编写服务端对客户端请求的具体逻辑。
- `context`：提供关于 RPC 调用的上下文信息，可以用于传递额外的信息到客户端或调整 RPC 行为。
- `request`：客户端发送的请求消息，这里是 `MsgRequest` 类型。
- `reply`：用于存储服务端发送给客户端的响应消息，这里是 `MsgResponse` 类型。
- `override`：表示这个函数是从基类继承并进行了重写。

**响应设置**：`reply->set_sum(request->num1() + request->num2());`

- 在 `reply` 中设置响应的数据，这里是将两个请求参数相加并赋值给 `sum`。

**返回状态**：`return Status::OK;`

- 表示 RPC 调用成功，返回一个 OK 状态。

  

```c++
void RunServer() {
  // 指定服务器地址（0.0.0.0:50051）
  std::string server_address("0.0.0.0:50051");

  // 创建 MyMsgService 类的实例（自定义服务的实现）
  MyMsgService service;

  // 创建一个 gRPC 服务器构建器
  ServerBuilder builder;

  // 在给定地址上监听，没有使用任何身份验证机制。
  builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());

  // 注册service作为我们与客户端通信的实例。
  // 在这种情况下，它对应一个同步服务。
  builder.RegisterService(&service);
  std::unique_ptr<Server> server(builder.BuildAndStart());

  // 打印消息
  std::cout << "Server listening on " << server_address << std::endl;

  // 等待服务器关闭。请注意，为了使此调用返回，必须有其他线程负责关闭服务器。
  server->Wait();
}
```

**服务器地址**：`std::string server_address("0.0.0.0:50051");`

- 指定服务器将监听的 IP 地址和端口。在这里，设置为 "0.0.0.0:50051"，表示它将在所有可用的网络接口上监听。

**服务实例**：`MyMsgService service;`

- 创建了 `MyMsgService` 类的实例，这是 gRPC 服务的实现。

**服务器构建器**：`ServerBuilder builder;`

- 创建了一个 gRPC 服务器构建器，用于配置和构建服务器。

**监听端口**：`builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());`

- 配置服务器在指定地址（`server_address`）上监听，没有使用任何身份验证机制（`grpc::InsecureServerCredentials()`）。在生产环境中应替换为安全凭证。

**服务注册**：`builder.RegisterService(&service);`

- 注册 `service` 实例，作为服务器与客户端通信的实现。

**服务器构建**：`std::unique_ptr<Server> server(builder.BuildAndStart());`

- 根据提供的配置组装服务器并启动。

**服务器监听消息**：`std::cout << "Server listening on " << server_address << std::endl;`

- 打印消息，指示服务器正在监听。

**服务器等待**：`server->Wait();`

- 阻塞主线程，等待服务器关闭。请注意，为了使 `Wait()` 返回，必须有另一个线程或机制负责关闭服务器。

#### 3.客户端

```c++
class MsgServiceClient {
 public:
  // 构造函数，接收 gRPC 通道的共享指针，并初始化 stub_
  MsgServiceClient(std::shared_ptr<Channel> channel)
      : stub_(MsgService::NewStub(channel)) {}

  // 客户端通过调用 GetMsg 函数与服务端通信
  MsgResponse GetMsg(int num1, int num2) {
    // 请求数据数据格式化到request
    MsgRequest request; 
    request.set_num1(num1);
    request.set_num2(num2);

    // 服务器返回端
    MsgResponse reply;

    // 客户端上下文。它可以用来传递额外的信息
    ClientContext context;

    // 实际的RPC调用
    Status status = stub_->GetMsg(&context, request, &reply);

    // 根据调用的状态进行处理
    if (status.ok()) {
      return reply;  // reply.sum();
    } else {
      std::cout << status.error_code() << ": " << status.error_message()
                << std::endl;
      return reply;
    }
  }

 private:
  // gRPC 自动生成的服务 stub
  std::unique_ptr<MsgService::Stub> stub_;
};
```

**构造函数**：接收一个 gRPC 通道的共享指针，并初始化 `stub_`，该 `stub_` 是由 gRPC 自动生成的服务 stub。这个 stub 用于发起 RPC 调用。

**GetMsg 函数**：通过调用这个函数与服务端通信。它接收两个整数参数 `num1` 和 `num2`，将它们格式化为 `MsgRequest` 对象，然后发起 gRPC 调用。

- `MsgRequest` 和 `MsgResponse` 是你在 protobuf 文件中定义的消息类型，分别用于请求和响应。
- `ClientContext` 是客户端上下文，可以用来传递额外的信息到服务器或调整 RPC 行为。
- `Status` 是 gRPC 提供的状态对象，用于检查 RPC 调用的结果。

**返回值**：如果 RPC 调用成功 (`status.ok()`)，则返回从服务器接收到的响应 (`reply`)。如果调用失败，打印错误信息并返回一个空的响应或错误状态。

#### 4.CMakeLists.txt

CMakeLists.txt文件可以根据官方的文件来进行稍微的修改就可以直接使用

这里主要注意要将grpc/examples/cpp下的cmake文件夹复制到项目路径下，然后在自己的CMakelist里面包含这个文件夹，以确保cmake命令的正确执行。

#### 5.编译运行示例代码

```bash
$ mkdir build
$ cd build
$ cmake ..
$ make
```


